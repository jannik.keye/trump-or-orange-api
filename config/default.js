module.exports = {
  server: {
    port: process.env.PORT || 3000
  },
  image: {
    width: 200,
    height: 200
  }
}