'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.allowCORS = exports.process = undefined;

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _classifier = require('../lib/classifier');

var _util = require('util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const process = exports.process = async (ctx, next) => {
  if ('POST' !== ctx.method) return await next();

  const filePath = ctx.request.body.files.file.path;

  const buffer = await (0, _util.promisify)(_fs2.default.readFile)(filePath);
  const result = await _classifier.classifier.classify(buffer);

  ctx.set('Content-Type', 'application/json');
  ctx.body = {
    trump: result[0],
    orange: result[1],
    other: result[2]
  };
};

const allowCORS = exports.allowCORS = async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*');
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS');
  ctx.set('Access-Control-Allow-Headers', 'content-Type, authorization');

  if (!ctx.get('Access-Control-Request-Method')) {
    await next();
  } else {
    ctx.status = 204;
  }
};
//# sourceMappingURL=middleware.js.map