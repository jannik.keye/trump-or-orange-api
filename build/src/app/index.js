'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _koa = require('koa');

var _koa2 = _interopRequireDefault(_koa);

var _koaBody = require('koa-body');

var _koaBody2 = _interopRequireDefault(_koaBody);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _middleware = require('./middleware');

var middleware = _interopRequireWildcard(_middleware);

var _url = require('url');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const app = new _koa2.default();

const _setup = () => {
  app.use(middleware.allowCORS);
  app.use((0, _koaBody2.default)({
    multipart: true
  }));
  app.use(middleware.process);
};

const _listen = () => app.listen(_config2.default.server.port);

exports.default = () => {
  _setup();
  _listen();
  console.log('App:', `listening on port ${_config2.default.server.port}`);
};
//# sourceMappingURL=index.js.map