'use strict';

var _index = require('./app/index');

var _index2 = _interopRequireDefault(_index);

var _init = require('./init/init');

var _init2 = _interopRequireDefault(_init);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _index2.default)();
(0, _init2.default)();
//# sourceMappingURL=index.js.map