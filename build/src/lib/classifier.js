'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.classifier = undefined;

var _synaptic = require('synaptic');

var _image = require('./image');

var image = _interopRequireWildcard(_image);

var _trump = require('../data/trump.json');

var _trump2 = _interopRequireDefault(_trump);

var _orange = require('../data/orange.json');

var _orange2 = _interopRequireDefault(_orange);

var _other = require('../data/other.json');

var _other2 = _interopRequireDefault(_other);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

class Classifier {
  constructor(config) {
    this.nn = new _synaptic.Architect.Perceptron(64, 32, 3);
    this.trainer = new _synaptic.Trainer(this.nn);
    this.trainingOptions = {
      rate: 0.05,
      iterations: 55,
      error: 0.002,
      log: 10
    };

    const trainingDataTrump = _trump2.default.map(d => ({
      input: d,
      output: [1, 0, 0] // is trump
    }));

    const trainingDataOrange = _orange2.default.map(d => ({
      input: d,
      output: [0, 1, 0] // is orange
    }));

    const trainingDataOther = _other2.default.slice(100, 116).map(d => ({
      input: d,
      output: [0, 0, 1]
    }));

    this.trainingData = trainingDataOrange.concat(trainingDataTrump, trainingDataOther);
  }

  train() {
    this.trainer.train(this.trainingData, this.trainingOptions);
  }

  async classify(buffer) {

    const processed = await image.process(buffer);
    const result = this.nn.activate(processed);

    return result;
  }
}

const classifier = exports.classifier = new Classifier();

exports.default = Classifier;
//# sourceMappingURL=classifier.js.map