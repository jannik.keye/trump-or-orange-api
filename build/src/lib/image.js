'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createNormalizedBins = exports.getBins = exports.pxToRangeArray = exports.colorValueArray = exports.binPixelBuffer = exports.binning = exports.isBin8 = exports.isBin7 = exports.isBin6 = exports.isBin5 = exports.isBin4 = exports.isBin3 = exports.isBin2 = exports.isBin1 = exports.isHigh = exports.isLow = exports.toPixelBuffer = undefined;
exports.asyncGetPixels = asyncGetPixels;
exports.process = process;

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _getPixels = require('get-pixels');

var _getPixels2 = _interopRequireDefault(_getPixels);

var _sharp = require('sharp');

var _sharp2 = _interopRequireDefault(_sharp);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _imageType = require('image-type');

var _imageType2 = _interopRequireDefault(_imageType);

var _ndarrayUnpack = require('ndarray-unpack');

var _ndarrayUnpack2 = _interopRequireDefault(_ndarrayUnpack);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function loadLocalImage(url) {
  return new Promise((resolve, reject) => {
    _fs2.default.readFile(_path2.default.resolve(__dirname, url), (err, data) => {
      if (err) reject(err);

      resolve(data);
    });
  });
}

async function asyncGetPixels(input, type) {
  let buffer = input;
  if (typeof input === 'string') {
    buffer = await loadLocalImage(input);
  }
  return new Promise((resolve, reject) => {
    (0, _getPixels2.default)(buffer, type, (err, pixels) => {
      if (err) {
        reject(err);
      }

      resolve(pixels);
    });
  });
}

async function processImage(buffer) {
  return (0, _sharp2.default)(buffer).jpeg().resize(_config2.default.image.width, _config2.default.image.height).toBuffer();
}

function meta(buffer) {
  return (0, _sharp2.default)(buffer).metadata(buffer);
}

const toPixelBuffer = exports.toPixelBuffer = async input => {
  let buffer = input;
  if (typeof input === 'string') {
    buffer = await loadLocalImage(input);
  }
  const processed = await processImage(buffer);

  // fs.writeFile(path.join(__dirname, '../../data/processed.jpg'), processed)
  const metaData = await meta(processed);
  const ndarray = await asyncGetPixels(processed, 'image/jpeg');

  return ndarray;
};

const isLow = exports.isLow = value => value >= 0 && value <= 127;
const isHigh = exports.isHigh = value => value > 127 && value <= 255;
const isBin1 = exports.isBin1 = px => isLow(px[0]) && isLow(px[1]) && isLow(px[2]);
const isBin2 = exports.isBin2 = px => isLow(px[0]) && isLow(px[1]) && isHigh(px[2]);
const isBin3 = exports.isBin3 = px => isLow(px[0]) && isHigh(px[1]) && isLow(px[2]);
const isBin4 = exports.isBin4 = px => isLow(px[0]) && isHigh(px[1]) && isHigh(px[2]);
const isBin5 = exports.isBin5 = px => isHigh(px[0]) && isLow(px[1]) && isLow(px[2]);
const isBin6 = exports.isBin6 = px => isHigh(px[0]) && isLow(px[1]) && isHigh(px[2]);
const isBin7 = exports.isBin7 = px => isHigh(px[0]) && isHigh(px[1]) && isLow(px[2]);
const isBin8 = exports.isBin8 = px => isHigh(px[0]) && isHigh(px[1]) && isHigh(px[2]);

const binning = exports.binning = bins => {
  return px => {
    if (isBin1(px)) bins[0]++;
    if (isBin2(px)) bins[1]++;
    if (isBin3(px)) bins[2]++;
    if (isBin4(px)) bins[3]++;
    if (isBin5(px)) bins[4]++;
    if (isBin6(px)) bins[5]++;
    if (isBin7(px)) bins[6]++;
    if (isBin8(px)) bins[7]++;
  };
};

const binPixelBuffer = exports.binPixelBuffer = async flatRGBArray => {
  const px = flatRGBArray.length;
  const bins = [0, 0, 0, 0, 0, 0, 0, 0];

  flatRGBArray.forEach(binning(bins));

  return bins.map(b => b / px);
};

// export async function process (url) {
//   const ndArray = await toPixelBuffer(url)
//   const rgbArray = await toRGBArray(ndArray)
//   const bins = binPixelBuffer(rgbArray)

//   return bins
// }

/**
 * 
 * @param {Number} value - value to determine range for
 * @param {Number} ranges - possible number of ranges the value can be in
 */
const colorValueArray = exports.colorValueArray = (value, ranges) => {
  const frac = 255 / ranges;
  const binary = Array.from({ length: ranges / 2 }, () => 0);

  for (let i = 0; i < ranges; i++) {
    const low = i * frac;
    const high = frac + i * frac;

    if (value >= low && value <= high) {
      if (i === 0) binary[0] = 0;
      if (i === 1) binary[0] = 1;
      if (i === 2) {
        binary[0] = 1;
        binary[1] = 0;
      }
      if (i === 3) {
        binary[0] = 1;
        binary[1] = 1;
      }
    }
  }

  return binary;
};

const pxToRangeArray = exports.pxToRangeArray = (pixel, ranges) => {
  const colorsBinary = pixel.map(p => colorValueArray(p, ranges)).reduce((p, c) => p.concat(c), []);

  return colorsBinary;
};

const getBins = exports.getBins = () => {
  let bins = [];
  for (let i = 0; i < 64; i++) {
    bins.push([i / 32 % 2, i / 16 % 2, i / 8 % 2, i / 4 % 2, i / 2 % 2, i % 2].map(e => Math.floor(e)));
  }

  return bins;
};

const createNormalizedBins = exports.createNormalizedBins = pixelArray => {
  const ranges = 4;
  const px = pixelArray.length;
  const bins = getBins();
  const rangeArrays = pixelArray.map(pixel => pxToRangeArray(pixel, ranges));
  const binCounts = Array.from({ length: bins.length }, () => 0);

  rangeArrays.forEach(range => {
    const binIndex = bins.findIndex(b => {
      let isEqual = true;
      b.forEach((v, i) => {
        if (v !== range[i]) {
          isEqual = false;
        }
      });

      return isEqual;
    });

    binCounts[binIndex]++;
  });

  return binCounts.map(b => b / px);
};

async function process(url) {
  const ndArray = await toPixelBuffer(url);
  const rgbArray = (0, _ndarrayUnpack2.default)(ndArray);
  const pixels = [];

  for (let x = 0; x < rgbArray.length; x++) {
    for (let y = 0; y < rgbArray[0].length; y++) {
      pixels.push(rgbArray[y][x]);
    }
  }

  const normalizedBins = createNormalizedBins(pixels);

  return normalizedBins;
}
//# sourceMappingURL=image.js.map