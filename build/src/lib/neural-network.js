'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _synaptic = require('synaptic');

var _trump = require('../../data/images/trump.json');

var _trump2 = _interopRequireDefault(_trump);

var _orange = require('../../data/images/orange.json');

var _orange2 = _interopRequireDefault(_orange);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const nn = new _synaptic.Architect.Perceptron(64, 10, 10, 10, 2);
const trainer = new _synaptic.Trainer(nn);

const trainingDataTrump = _trump2.default.map(d => ({
  input: d,
  output: [1, 0] // is trump
}));

const trainingDataOrange = _orange2.default.map(d => ({
  input: d,
  output: [0, 1] // is orange
}));

const trainingOptions = {
  rate: 0.5,
  iterations: 10000,
  error: 0.005
};

console.log('training');
trainer.train(trainingDataTrump.concat(trainingDataOrange), trainingOptions);
console.log('training done');

exports.default = nn;
//# sourceMappingURL=neural-network.js.map