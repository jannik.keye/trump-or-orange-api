'use strict';

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _googleImages = require('google-images');

var _googleImages2 = _interopRequireDefault(_googleImages);

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _util = require('util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const req = _requestPromise2.default.defaults({ encoding: null });

const client = new _googleImages2.default(process.env.CSEID, process.env.CSE_API_KEY);

const pth = p => _path2.default.join(__dirname, p);

async function searchImages(term, options) {
  const data = await client.search(term, options);

  return data;
}

async function fetchImages(objects, folder, prefix) {
  const buffers = await Promise.all(objects.map((obj, i) => {
    return req(obj.url);
  }));

  buffers.forEach((buffer, i) => {
    _fs2.default.writeFileSync(pth(`../../data/images/${folder}/${prefix}_${i}.jpg`), buffer);
  });
}

async function runOrange() {
  const a = Array.from({ length: 1 });

  const resultsArrays = await Promise.all(a.map((e, i) => {
    return searchImages('oranges fruit', { page: i + 38 });
  }));

  await Promise.all(resultsArrays.map((r, i) => {
    return fetchImages(r, 'orange', `orange_${i + 37}`);
  }));
}
async function runTrump() {
  const a = Array.from({ length: 1 });

  const resultsArrays = await Promise.all(a.map((e, i) => {
    return searchImages('donald trump', { page: i + 51 });
  }));

  await Promise.all(resultsArrays.map((r, i) => {
    return fetchImages(r, 'trump', `trump_${i + 50}`);
  }));
}

// runOrange()
runTrump();
//# sourceMappingURL=fetch-images.js.map