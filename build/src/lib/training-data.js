'use strict';

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _image = require('./image');

var img = _interopRequireWildcard(_image);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

async function generateTrainingData(type) {
  if (!['trump', 'orange', 'other'].includes(type)) throw Error('invalid training data type');

  const folderUrl = _path2.default.join(__dirname, `../../data/images/${type}`);
  const fileNames = _fs2.default.readdirSync(folderUrl);

  fileNames.splice(0, 1);

  const bins = await Promise.all(fileNames.map(f => img.process(folderUrl + '/' + f)));

  _fs2.default.writeFileSync(_path2.default.resolve(__dirname, '../data' + `/${type}.json`), JSON.stringify(bins));

  return bins;
}

// generateTrainingData('trump')
// generateTrainingData('orange')
generateTrainingData('other');
//# sourceMappingURL=training-data.js.map