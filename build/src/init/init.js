'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = init;

var _classifier = require('../lib/classifier');

function init() {
  _classifier.classifier.train();
}
//# sourceMappingURL=init.js.map