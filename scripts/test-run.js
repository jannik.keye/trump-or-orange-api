import path from 'path'
import fs from 'fs'
import {promisify} from 'util'
import Classifier from '../src/lib/classifier'

const classifier = new Classifier()

classifier.train()

async function testTrump () {
  let count = 100
  let correct = 0
  let wrong = 0
  const files = await promisify(fs.readdir)(path.join(__dirname, '../data/images/trump'))
  files.splice(0, 1)

  const results = await Promise.all(files.slice(0, 100).map(async file => {
    const buffer = await promisify(fs.readFile)(path.join(__dirname, '../data/images/trump/' + file))
    const result = await classifier.classify(buffer)
    return result
  }))

  results.forEach(r => {
    if (r[0] > r[1] && r[0] > r[2]) {
      correct++
    } else if (r[1] > r[0] || r[2] > r[0]) {
      wrong++
    }
  })

  console.log('Trump correct:', correct + '/' + count)
  console.log('Trump wrong:', wrong + '/' + count)
}

async function testOther () {
  let count = 100
  let correct = 0
  let wrong = 0
  const files = await promisify(fs.readdir)(path.join(__dirname, '../data/images/other'))
  files.splice(0, 1)

  const results = await Promise.all(files.slice(0, 100).map(async file => {
    const buffer = await promisify(fs.readFile)(path.join(__dirname, '../data/images/other/' + file))
    const result = await classifier.classify(buffer)
    return result
  }))

  results.forEach(r => {
    if (r[2] > r[0] && r[2] > r[1]) {
      correct++
    } else if (r[1] > r[2] || r[0] > r[2]) {
      wrong++
    }
  })

  console.log('Other correct:', correct + '/' + count)
  console.log('Other wrong:', wrong + '/' + count)
}

async function testOrange () {
  let count = 100
  let correct = 0
  let wrong = 0
  const files = await promisify(fs.readdir)(path.join(__dirname, '../data/images/orange'))
  files.splice(0, 1)

  const results = await Promise.all(files.slice(0, 100).map(async file => {
    const buffer = await promisify(fs.readFile)(path.join(__dirname, '../data/images/orange/' + file))
    const result = await classifier.classify(buffer)
    return result
  }))

  results.forEach(r => {
    if (r[1] > r[0] && r[1] > r[2]) {
      correct++
    } else if (r[1] < r[0] || r[1] < r[2]) {
      wrong++
    }
  })

  console.log('Orange correct:', correct + '/' + count)
  console.log('Orange wrong:', wrong + '/' + count)
}

testTrump()
testOrange()
testOther()