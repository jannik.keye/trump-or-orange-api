import fileType from 'file-type'
import { promisify } from 'util';
import fs from 'fs';
import { resolve } from 'path';

const relPath = '../data/images/other'

const emptyImageBuffer = fs.readFileSync(resolve(__dirname, '../data/images/check_remove.jpg'))

function isJpg (buffer) {
  const result = fileType(buffer)

  return result ? result.mime === 'image/jpeg' : false
}

function isEmptyImage (buffer) {
  return Buffer.compare(buffer, emptyImageBuffer)
}

async function clean (fileNames) {
  fileNames.forEach((file) => {
    const fileUrl = resolve(__dirname, relPath + '/' + file)
    const buffer = fs.readFileSync(fileUrl)

    if (!isJpg(buffer.slice(0, 4100))) {
      fs.unlinkSync(fileUrl)
    }
  })
}


const files = promisify(fs.readdir)(resolve(__dirname, relPath))
  .then(async names => {
    await clean(names)
  })
