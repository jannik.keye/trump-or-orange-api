import fs from 'fs'
import p from 'path'
import request from 'request-promise'
import { promisify } from 'util';

const req = request.defaults({ encoding: null })

const dataPath = '../data/image_net_part.txt'
const outputPath = '../data/images/other'

async function loadFileString (relPath) {
  const path = p.resolve(__dirname, relPath)
  const content = await promisify(fs.readFile)(path)
  const string = content.toString()

  return string
}

async function getImageUrls (string) {
  const urls = string
    .split(/\n/g)
    .map(line => line.split(/\t/))
    .map(array => array[1])

  return urls
} 

async function loadImages (urls, prefix) {
  const promises = urls.map(async (url, i) => {
    try {
      const buffer = await req(url)
      const path = p.resolve(__dirname, outputPath + `/${prefix}_${i}.jpg`)
  
      console.log(i)

      return promisify(fs.writeFile)(path, buffer)
    } catch (e) {
      return new Promise(resolve => resolve())
    }
  })

  return Promise.all(promises)
}

async function run (prefix) {
  const file = await loadFileString(dataPath)
  const urls = await getImageUrls(file, prefix)
  
  await loadImages(urls)

  console.log('Done.')
}

run('other')