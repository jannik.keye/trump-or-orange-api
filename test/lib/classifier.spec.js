import fs from 'fs'
import path from 'path'

import Classifier from '../../src/lib/classifier'
import { promisify } from 'util';
import request from 'request-promise'

const req = request.defaults({ encoding: null })

jest.setTimeout = 60000
describe('Classifier', () => {
  let classifier = null

  beforeAll(() => {
    classifier = new Classifier()
    classifier.train()
  })

  it('recognize trump images', async () => {
    const files = await promisify(fs.readdir)(path.join(__dirname, '../../data/images/trump'))
    files.splice(0, 1)

    const results = await Promise.all(files.slice(250, 350).map(async file => {
      const buffer = await promisify(fs.readFile)(path.join(__dirname, '../../data/images/trump/' + file))
      const result = await classifier.classify(buffer)
      return result
    }))

    const realResults = []

    results.forEach(r => {
      if (r[0] > r[1] && r[0] > r[2]) {
        realResults.push(true)
      } else {
        realResults.push(false)
      }
    })

    const falseResults = realResults.filter(r => !r)

    expect(falseResults.length).toEqual(0)
  }, 30000)

  it('recognize orange images', async () => {
    const files = await promisify(fs.readdir)(path.join(__dirname, '../../data/images/orange'))
    files.splice(0, 1)

    const results = await Promise.all(files.slice(100, 200).map(async file => {
      const buffer = await promisify(fs.readFile)(path.join(__dirname, '../../data/images/orange/' + file))
      const result = await classifier.classify(buffer)
      return result
    }))
    
    const realResults = []

    results.forEach(r => {
      if (r[1] > r[0] && r[1] > r[2]) {
        realResults.push(true)
      } else {
        realResults.push(false)
      }
    })

    const falseResults = realResults.filter(r => !r)

    expect(falseResults.length).toEqual(0)
  }, 30000)

  it('recognizes spiegel online trump image as trump', async () => {
    const buffer = await req('http://cdn1.spiegel.de/images/image-1237368-860_galleryfree-nysr-1237368.jpg')
    const result = await classifier.classify(buffer)

    expect(result[0]).toBeGreaterThan(result[1])
    expect(result[0]).toBeGreaterThan(result[2])
  })

  it('recognizes orange slices image as orange', async () => {
    const buffer = await req('https://previews.123rf.com/images/spectr/spectr1006/spectr100600003/7098747-fresh-and-big-orange-slices-background-Stock-Photo.jpg')
    const result = await classifier.classify(buffer)

    expect(result[1]).toBeGreaterThan(result[0])
    expect(result[1]).toBeGreaterThan(result[2])
  })

  it('recognizes orange trump image as trump', async () => {
    const buffer = await req('http://brewminate.com/wp-content/uploads/2016/05/Trump71.jpg')
    const result = await classifier.classify(buffer)

    expect(result[0]).toBeGreaterThan(result[1])
    expect(result[0]).toBeGreaterThan(result[2])
  })

  it('should detect plain orange square as orange', async () => {
    const buffer = await promisify(fs.readFile)(path.join(__dirname, '../../data/Orange.png'))
    const result = await classifier.classify(buffer)
    expect(result[1]).toBeGreaterThan(result[0])
    expect(result[1]).toBeGreaterThan(result[2])
  })

  it('should detect trump', async () => {
    const buffer = await req('https://cdn.theatlantic.com/assets/media/img/mt/2018/01/RTX47GSS/hero_wide_640.jpg?1515723594')
    const result = await classifier.classify(buffer)
    expect(result[0]).toBeGreaterThan(result[1])
    expect(result[0]).toBeGreaterThan(result[2])
  })

  it('should detect orange', async () => {
    const buffer = await req('http://orange.bilderu.de/bilder/orange-c-6.jpg')
    const result = await classifier.classify(buffer)
    expect(result[1]).toBeGreaterThan(result[0])
    expect(result[1]).toBeGreaterThan(result[2])
  })

  it('should detect other images', async () => {
    const buffer = await req('https://www.welt.de/img/wissenschaft/mobile157999786/1652508547-ci102l-w1024/Giraffen-2.jpg')
    const result = await classifier.classify(buffer)
    expect(result[2]).toBeGreaterThan(result[0])
    expect(result[2]).toBeGreaterThan(result[1])
  })
})