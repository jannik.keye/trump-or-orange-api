import { classifier } from '../lib/classifier'

export default function init () {
  classifier.train()
}

