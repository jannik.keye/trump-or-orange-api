import { Architect, Trainer } from 'synaptic'
import * as image from './image'
import trumpData from '../data/trump.json'
import orangeData from '../data/orange.json'
import otherData from '../data/other.json'

class Classifier {
  constructor(config) {
    this.nn = new Architect.Perceptron(64, 32, 3)
    this.trainer = new Trainer(this.nn)
    this.trainingOptions = {
      rate: 0.05,
      iterations: 55,
      error: 0.002,
      log: 10
    }

    const trainingDataTrump = trumpData.map(d => ({
      input: d,
      output: [1, 0, 0] // is trump
    }))
    
    const trainingDataOrange = orangeData.map(d => ({
      input: d,
      output: [0, 1, 0] // is orange
    }))

    const trainingDataOther = otherData.slice(100, 116).map(d => ({
      input: d,
      output: [0, 0, 1]
    }))

    this.trainingData = trainingDataOrange.concat(trainingDataTrump, trainingDataOther)
  }

  train() {
    this.trainer.train(this.trainingData, this.trainingOptions)
  }

 async classify(buffer) {

    const processed = await image.process(buffer)
    const result = this.nn.activate(processed)

    return result
  }
}

export const classifier = new Classifier()

export default Classifier