import request from 'request-promise'
import fs from 'fs'
import path from 'path'
import GoogleImages from 'google-images'

import config from 'config'
import { promisify } from 'util';

const req = request.defaults({ encoding: null })

const client = new GoogleImages(process.env.CSEID, process.env.CSE_API_KEY);

const pth = (p) => path.join(__dirname, p)

async function searchImages(term, options) {
  const data = await client.search(term, options)

  return data
}

async function fetchImages (objects, folder, prefix) {
  const buffers = await Promise.all(objects.map((obj, i) => {
    return req(obj.url)
  }))

  buffers.forEach((buffer, i) => {
    fs.writeFileSync(pth(`../../data/images/${folder}/${prefix}_${i}.jpg`), buffer)
  })
}

async function runOrange () {
  const a = Array.from({ length: 1 })


  const resultsArrays = await Promise.all(a.map((e, i) => {
    return searchImages('oranges fruit', { page: i + 38 })
  }))

  await Promise.all(resultsArrays.map((r, i) => {
    return fetchImages(r, 'orange', `orange_${i + 37}`)
  }))
}
async function runTrump() {
  const a = Array.from({ length: 1 })

  const resultsArrays = await Promise.all(a.map((e, i) => {
    return searchImages('donald trump', { page: i + 51 })
  }))

  await Promise.all(resultsArrays.map((r, i) => {
    return fetchImages(r, 'trump', `trump_${i + 50}`)
  }))
}

// runOrange()
runTrump()