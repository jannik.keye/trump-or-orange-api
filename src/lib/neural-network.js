import { Architect, Trainer } from 'synaptic'
import trumpData from '../../data/images/trump.json'
import orangeData from '../../data/images/orange.json'

const nn = new Architect.Perceptron(64, 10, 10, 10, 2)
const trainer = new Trainer(nn)

const trainingDataTrump = trumpData.map(d => ({
  input: d,
  output: [1, 0] // is trump
}))

const trainingDataOrange = orangeData.map(d => ({
  input: d,
  output: [0, 1] // is orange
}))

const trainingOptions = {
  rate: 0.5,
  iterations: 10000,
  error: 0.005
}


console.log('training')
trainer.train(trainingDataTrump.concat(trainingDataOrange), trainingOptions);
console.log('training done')

export default nn
