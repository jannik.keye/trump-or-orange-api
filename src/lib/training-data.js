import fs from 'fs'
import path from 'path'
import * as img from './image'

async function generateTrainingData (type) {
  if (!['trump', 'orange', 'other'].includes(type)) throw Error('invalid training data type')

  const folderUrl = path.join(__dirname, `../../data/images/${type}`)
  const fileNames = fs.readdirSync(folderUrl)

  fileNames.splice(0, 1)

  const bins = await Promise.all(fileNames.map(f => img.process(folderUrl + '/' + f)))

  fs.writeFileSync(path.resolve(__dirname, '../data' + `/${type}.json`), JSON.stringify(bins))

  return bins
}

// generateTrainingData('trump')
// generateTrainingData('orange')
generateTrainingData('other')