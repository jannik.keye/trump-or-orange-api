import config from 'config'
import request from 'request-promise'
import getPixels from 'get-pixels'
import sharp from 'sharp'
import fs from 'fs'
import path from 'path'
import imageType from 'image-type'
import unpack from 'ndarray-unpack'

const req = request.defaults({ encoding: null })

function loadLocalImage (url) {
  return new Promise((resolve, reject) => {
    fs.readFile(path.resolve(__dirname, url), (err, data) => {
      if (err) reject(err)

      resolve(data)
    })
  })
}

function loadImage (url) {
  const isRemote = /(http\:\/\/)|(https\:\/\/)/.test(url)
  
  return isRemote ? loadRemoteImage(url) : loadLocalImage(url)
}

function loadRemoteImage (url) {
  return req(url)
}

export async function asyncGetPixels (input, type) {
  let buffer = input
  if (typeof input === 'string') {
    buffer = await loadLocalImage(input)
  }
  return new Promise((resolve, reject) => {
    getPixels(buffer, type, (err, pixels) => {
      if(err) {
        reject(err)
      }

      resolve(pixels)
    })
  })
}

async function processImage(buffer) {
  return sharp(buffer)
    .jpeg()
    .resize(config.image.width, config.image.height)
    .toBuffer()
}

function meta (buffer) {
  return sharp(buffer).metadata(buffer)
}

export const toPixelBuffer = async (input) => {
  let buffer = input
  if (typeof input === 'string') {
    buffer = await loadImage(input)
  }
  const processed = await processImage(buffer)

  // fs.writeFile(path.join(__dirname, '../../data/processed.jpg'), processed)
  const metaData = await meta(processed)
  const ndarray = await asyncGetPixels(processed, 'image/jpeg')

  return ndarray
}

export const isLow = value => value >= 0 && value <= 127
export const isHigh = value => value > 127 && value <= 255
export const isBin1 = px => isLow(px[0]) && isLow(px[1]) && isLow(px[2])
export const isBin2 = px => isLow(px[0]) && isLow(px[1]) && isHigh(px[2])
export const isBin3 = px => isLow(px[0]) && isHigh(px[1]) && isLow(px[2])
export const isBin4 = px => isLow(px[0]) && isHigh(px[1]) && isHigh(px[2])
export const isBin5 = px => isHigh(px[0]) && isLow(px[1]) && isLow(px[2])
export const isBin6 = px => isHigh(px[0]) && isLow(px[1]) && isHigh(px[2])
export const isBin7 = px => isHigh(px[0]) && isHigh(px[1]) && isLow(px[2])
export const isBin8 = px => isHigh(px[0]) && isHigh(px[1]) && isHigh(px[2])

export const binning = (bins) => {
  return (px) => {
    if (isBin1(px)) bins[0]++
    if (isBin2(px)) bins[1]++
    if (isBin3(px)) bins[2]++
    if (isBin4(px)) bins[3]++
    if (isBin5(px)) bins[4]++
    if (isBin6(px)) bins[5]++
    if (isBin7(px)) bins[6]++
    if (isBin8(px)) bins[7]++
  }
}

export const binPixelBuffer = async (flatRGBArray) => {
  const px = flatRGBArray.length
  const bins = [0, 0, 0, 0, 0, 0, 0, 0]

  flatRGBArray.forEach(binning(bins))

  return bins.map(b => b / px)
}

// export async function process (url) {
//   const ndArray = await toPixelBuffer(url)
//   const rgbArray = await toRGBArray(ndArray)
//   const bins = binPixelBuffer(rgbArray)

//   return bins
// }

/**
 * 
 * @param {Number} value - value to determine range for
 * @param {Number} ranges - possible number of ranges the value can be in
 */
export const colorValueArray = (value, ranges) => {
  const frac = 255 / ranges
  const binary = Array.from({ length: ranges / 2 }, () => 0)

  for (let i = 0; i < ranges ; i++) {
    const low = i * frac
    const high = frac + i * frac

    
    if (value >= low && value <= high) {
      if (i === 0) binary[0] = 0
      if (i === 1) binary[0] = 1
      if (i === 2) {
        binary[0] = 1
        binary[1] = 0
      }
      if (i === 3) {
        binary[0] = 1
        binary[1] = 1
      }
    }
  }

  return binary
}

export const pxToRangeArray = (pixel, ranges) => {
  const colorsBinary = pixel
    .map(p => colorValueArray(p, ranges))
    .reduce((p, c) => p.concat(c), [])

  return colorsBinary
}

export const getBins = () => {
  let bins = []
  for(let i = 0; i < 64; i++) {
    bins.push([i/32%2, i/16%2, i/8%2, i/4%2, i/2%2, i%2].map(e => Math.floor(e)));
  }

  return bins
}

export const createNormalizedBins = (pixelArray) => {
  const ranges = 4
  const px = pixelArray.length
  const bins = getBins()
  const rangeArrays = pixelArray.map(pixel => pxToRangeArray(pixel, ranges))
  const binCounts = Array.from({ length: bins.length }, () => 0)

  rangeArrays.forEach(range => {
    const binIndex = bins.findIndex(b => {
      let isEqual = true
      b.forEach((v, i) => {
        if (v !== range[i]) {
          isEqual = false
        }
      })

      return isEqual
    })

    binCounts[binIndex]++
  })

  return binCounts.map(b => b / px)
}

export async function process (url) {
  const ndArray = await toPixelBuffer(url)
  const rgbArray = unpack(ndArray)
  const pixels = []

  for (let x = 0; x < rgbArray.length; x++) {
    for (let y = 0; y < rgbArray[0].length; y++) {
      pixels.push(rgbArray[y][x])
    }
  }
  
  const normalizedBins = createNormalizedBins(pixels)

  return normalizedBins
}