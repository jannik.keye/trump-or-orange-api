import Koa from 'koa'
import koaBody from 'koa-body'
import config from 'config'

import * as middleware from './middleware'
import { resolve } from 'url';

const app = new Koa()

const _setup = () => {
  app.use(middleware.allowCORS)
  app.use(middleware.respondError)
  app.use(koaBody({
    multipart: true
  }))
  app.use(middleware.process)
}

const _listen = () => app.listen(config.server.port)

export default () => {
  _setup()
  _listen()
  console.log('App:', `listening on port ${config.server.port}`)
}