import fs from 'fs'
import { classifier } from '../lib/classifier'
import { promisify } from 'util';

export const process = async (ctx, next) => {
  if ('POST' !== ctx.method) return await next();
  let url = null

  if (ctx.request.body.files) {
    url = ctx.request.body.files.file.path
  }

  if (ctx.request.body.url) {
    url = ctx.request.body.url
  }

  if (!url) {
    ctx.throw(400, 'Please provide image url.')
  }

  const result = await classifier.classify(url)

  ctx.set('Content-Type', 'application/json')
  ctx.body = {
    trump: result[0],
    orange: result[1],
    other: result[2]
  }
}

export const allowCORS = async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*')
  ctx.set('Access-Control-Allow-Methods', 'POST, OPTIONS')
  ctx.set('Access-Control-Allow-Headers', 'content-Type, authorization')

  if (!ctx.get('Access-Control-Request-Method')) {
    await next()
  } else {
    ctx.status = 204
  }
}

export const respondError = async (ctx, next) => {
  const { request: { url, method, body } } = ctx
  try {
    await next()
  } catch (err) {
    ctx.status = err.status || 500
    ctx.body = err.message || ''
    ctx.app.emit('error', err, ctx)
  }
}